<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();

$packages_args = array(
	'post_type'      => 'events',
	'posts_per_page' => -1,
	'meta_key'			 => 'event_price',
	'orderby'			   => 'meta_value',
	'order'          => 'DESC',
	'tax_query' => array(
		array(
			'taxonomy' => 'event-category',
			'field'    => 'slug',
			'terms'    => 'packages',
		),
	),
);

$event = new WP_Query( $packages_args );

$context['post'] = $post;
$context['event'] = Timber::get_posts($event);

Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );