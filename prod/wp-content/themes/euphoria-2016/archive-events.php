<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'archive-events.twig', 'archive.twig', 'index.twig' );

$data = Timber::get_context();

$data['title'] = post_type_archive_title( '', false );
array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );

$prefest = array(
	'posts_per_page' => -1,
	'post_type' => 'events',
	'tax_query' => array(
		array(
			'taxonomy' => 'event-category',
			'field'    => 'slug',
			'terms'    => 'pre-festival',
		),
	),
);

$packages_args = array(
	'post_type'      => 'events',
	'posts_per_page' => -1,
	'meta_key'			 => 'event_price',
	'tax_query'      => array(
		array(
			'taxonomy' => 'event-category',
			'field'    => 'slug',
			'terms'    => 'packages',
		),
	),
);

$hotel_args = array(
	'post_type'      => 'events',
	'posts_per_page' => -1,
	'meta_key'			 => 'event_price',
	'tax_query'      => array(
		array(
			'taxonomy' => 'event-category',
			'field'    => 'slug',
			'terms'    => 'hotel-ticket-packages',
		),
	),
);

$thursday_args = array(
	'posts_per_page' => -1,
	'post_type' => 'events',
	'tax_query' => array(
		array(
			'taxonomy' => 'event-category',
			'field'    => 'slug',
			'terms'    => 'thursday',
		),
	),
);

$friday_args = array(
	'posts_per_page' => -1,
	'post_type' => 'events',
	'tax_query' => array(
		array(
			'taxonomy' => 'event-category',
			'field'    => 'slug',
			'terms'    => 'friday',
		),
	),
);

$saturday_args = array(
	'posts_per_page' => -1,
	'post_type' => 'events',
	'tax_query' => array(
		array(
			'taxonomy' => 'event-category',
			'field'    => 'slug',
			'terms'    => 'saturday',
		),
	),
);

$sunday_args = array(
	'posts_per_page' => -1,
	'post_type' => 'events',
	'tax_query' => array(
		array(
			'taxonomy' => 'event-category',
			'field'    => 'slug',
			'terms'    => 'sunday',
		),
	),
);



$prefest  = new WP_Query( $prefest );
$packages = new WP_Query( $packages_args );
$hotel    = new WP_Query( $hotel_args );
$thursday = new WP_Query( $thursday_args );
$friday   = new WP_Query( $friday_args );
$saturday = new WP_Query( $saturday_args );
$sunday   = new WP_Query( $sunday_args );

$data['events_page_subheading'] = get_field('events_page_subheading', 'options');

$data['posts']    = Timber::get_posts();
$data['prefest']  = Timber::get_posts($prefest);
$data['packages'] = Timber::get_posts($packages);
$data['hotel']    = Timber::get_posts($hotel);
$data['thursday'] = Timber::get_posts($thursday);
$data['friday']   = Timber::get_posts($friday);
$data['saturday'] = Timber::get_posts($saturday);
$data['sunday']   = Timber::get_posts($sunday);

Timber::render( $templates, $data );
