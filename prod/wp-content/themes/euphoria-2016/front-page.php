<?php
/**
 * front-page template file
 * Settings > Reading sets up Wordpress to use static front page
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();

$featured_events_args = array(
	'post_type'  => 'events',
	'meta_query' => array(
		array(
			'key'   => 'featured_event',
			'value' => true,
		),
	),
);

$featured_supporters_args = array(
	'post_type'  => 'supporters',
	'meta_query' => array(
		array(
			'key'   => 'featured_supporter',
			'value' => true,
		),
	),
);

$featured_events     = new WP_Query( $featured_events_args );
$featured_supporters = new WP_Query( $featured_supporters_args );

$context['featured_events']     = Timber::get_posts( $featured_events );
$context['featured_supporters'] = Timber::get_posts( $featured_supporters );
$context['homepage_quotes']     = get_field('homepage_quotes', 'options');
$context['hero_slides']         = get_field('homepage_hero_slider', 'options');
$context['event_date']         = get_field('event_date', 'options');

Timber::render( 'front-page.twig', $context );
