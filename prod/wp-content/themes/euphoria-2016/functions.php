<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );

		register_nav_menu( 'header-menu', __( 'Header Menu' ) );

		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );

		add_action( 'init', array( $this, 'add_options_page' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'init', array( $this, 'load_scripts' ));

		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types

		$venue_labels = array(
			'name'                => _x( 'Venues', 'Post Type General Name', 'euphoria-2016' ),
			'singular_name'       => _x( 'Venue', 'Post Type Singular Name', 'euphoria-2016' ),
			'menu_name'           => __( 'Venues', 'euphoria-2016' ),
			'parent_item_colon'   => __( 'Parent Venue', 'euphoria-2016' ),
			'all_items'           => __( 'All Venues', 'euphoria-2016' ),
			'view_item'           => __( 'View Venue', 'euphoria-2016' ),
			'add_new_item'        => __( 'Add New Venue', 'euphoria-2016' ),
			'add_new'             => __( 'Add New', 'euphoria-2016' ),
			'edit_item'           => __( 'Edit Venue', 'euphoria-2016' ),
			'update_item'         => __( 'Update Venue', 'euphoria-2016' ),
			'search_items'        => __( 'Search Venue', 'euphoria-2016' ),
			'not_found'           => __( 'Not Found', 'euphoria-2016' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'euphoria-2016' ),
		);

		$venue_args = array(
			'label'               => __( 'Venues', 'euphoria-2016' ),
			'description'         => __( 'euphoria Venues', 'euphoria-2016' ),
			'menu_icon'           => 'dashicons-location',
			'labels'              => $venue_labels,
			'supports'            => array( 'title' ),
			'taxonomies'          => array( '' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		$recipes_labels = array(
			'name'                => _x( 'Recipes', 'Post Type General Name', 'euphoria-2016' ),
			'singular_name'       => _x( 'Recipe', 'Post Type Singular Name', 'euphoria-2016' ),
			'menu_name'           => __( 'Recipes', 'euphoria-2016' ),
			'parent_item_colon'   => __( 'Parent Recipe', 'euphoria-2016' ),
			'all_items'           => __( 'All Recipes', 'euphoria-2016' ),
			'view_item'           => __( 'View Recipe', 'euphoria-2016' ),
			'add_new_item'        => __( 'Add New Recipe', 'euphoria-2016' ),
			'add_new'             => __( 'Add New', 'euphoria-2016' ),
			'edit_item'           => __( 'Edit Recipe', 'euphoria-2016' ),
			'update_item'         => __( 'Update Recipe', 'euphoria-2016' ),
			'search_items'        => __( 'Search Recipe', 'euphoria-2016' ),
			'not_found'           => __( 'Not Found', 'euphoria-2016' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'euphoria-2016' ),
		);

		$recipe_args = array(
			'label'               => __( 'Recipes', 'euphoria-2016' ),
			'description'         => __( 'euphoria Recipes and Sponsors', 'euphoria-2016' ),
			'menu_icon'           => 'dashicons-carrot',
			'labels'              => $recipes_labels,
			'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
			'taxonomies'          => array( 'recipe-category' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		$events_labels = array(
			'name'                => _x( 'Events', 'Post Type General Name', 'euphoria-2016' ),
			'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'euphoria-2016' ),
			'menu_name'           => __( 'Events', 'euphoria-2016' ),
			'parent_item_colon'   => __( 'Parent Event', 'euphoria-2016' ),
			'all_items'           => __( 'All Events', 'euphoria-2016' ),
			'view_item'           => __( 'View Event', 'euphoria-2016' ),
			'add_new_item'        => __( 'Add New Event', 'euphoria-2016' ),
			'add_new'             => __( 'Add New', 'euphoria-2016' ),
			'edit_item'           => __( 'Edit Event', 'euphoria-2016' ),
			'update_item'         => __( 'Update Event', 'euphoria-2016' ),
			'search_items'        => __( 'Search Event', 'euphoria-2016' ),
			'not_found'           => __( 'Not Found', 'euphoria-2016' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'euphoria-2016' ),
		);

		$event_args = array(
			'label'               => __( 'Events', 'euphoria-2016' ),
			'description'         => __( 'euphoria Events and Sponsors', 'euphoria-2016' ),
			'menu_icon'           => 'dashicons-tickets-alt',
			'labels'              => $events_labels,
			'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
			'taxonomies'          => array( 'event-category', 'ticket-packages', 'tag' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		$supporters_labels = array(
			'name'                => _x( 'Supporters', 'Post Type General Name', 'euphoria-2016' ),
			'singular_name'       => _x( 'Supporter', 'Post Type Singular Name', 'euphoria-2016' ),
			'menu_name'           => __( 'Supporters', 'euphoria-2016' ),
			'parent_item_colon'   => __( 'Parent Supporter', 'euphoria-2016' ),
			'all_items'           => __( 'All Supporters', 'euphoria-2016' ),
			'view_item'           => __( 'View Supporter', 'euphoria-2016' ),
			'add_new_item'        => __( 'Add New Supporter', 'euphoria-2016' ),
			'add_new'             => __( 'Add New', 'euphoria-2016' ),
			'edit_item'           => __( 'Edit Supporter', 'euphoria-2016' ),
			'update_item'         => __( 'Update Supporter', 'euphoria-2016' ),
			'search_items'        => __( 'Search Supporter', 'euphoria-2016' ),
			'not_found'           => __( 'Not Found', 'euphoria-2016' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'euphoria-2016' ),
		);

		$supporter_args = array(
			'label'               => __( 'Supporters', 'euphoria-2016' ),
			'description'         => __( 'euphoria Supporters and Sponsors', 'euphoria-2016' ),
			'menu_icon'           => 'dashicons-heart',
			'labels'              => $supporters_labels,
			'supports'            => array( 'title', 'author', 'thumbnail', 'revisions', 'custom-fields' ),
			'taxonomies'          => array( 'support-levels' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		$talent_labels = array(
			'name'                => _x( 'Talent', 'Post Type General Name', 'euphoria-2016' ),
			'singular_name'       => _x( 'Talent', 'Post Type Singular Name', 'euphoria-2016' ),
			'menu_name'           => __( 'Talent', 'euphoria-2016' ),
			'parent_item_colon'   => __( 'Parent Talent', 'euphoria-2016' ),
			'all_items'           => __( 'All Talents', 'euphoria-2016' ),
			'view_item'           => __( 'View Talent', 'euphoria-2016' ),
			'add_new_item'        => __( 'Add New Talent', 'euphoria-2016' ),
			'add_new'             => __( 'Add New', 'euphoria-2016' ),
			'edit_item'           => __( 'Edit Talent', 'euphoria-2016' ),
			'update_item'         => __( 'Update Talent', 'euphoria-2016' ),
			'search_items'        => __( 'Search Talent', 'euphoria-2016' ),
			'not_found'           => __( 'Not Found', 'euphoria-2016' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'euphoria-2016' ),
		);

		$talent_args = array(
			'label'               => __( 'Supporters', 'euphoria-2016' ),
			'description'         => __( 'euphoria Supporters and Sponsors', 'euphoria-2016' ),
			'menu_icon'           => 'dashicons-star-filled',
			'labels'              => $talent_labels,
			'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields' ),
			'taxonomies'          => array( 'talent-category' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		$staff_labels = array(
			'name'                => _x( 'Staff', 'Post Type General Name', 'euphoria-2016' ),
			'singular_name'       => _x( 'Staff', 'Post Type Singular Name', 'euphoria-2016' ),
			'menu_name'           => __( 'Staff', 'euphoria-2016' ),
			'parent_item_colon'   => __( 'Parent Staff', 'euphoria-2016' ),
			'all_items'           => __( 'All Staff', 'euphoria-2016' ),
			'view_item'           => __( 'View Staff', 'euphoria-2016' ),
			'add_new_item'        => __( 'Add New Staff', 'euphoria-2016' ),
			'add_new'             => __( 'Add New', 'euphoria-2016' ),
			'edit_item'           => __( 'Edit Staff', 'euphoria-2016' ),
			'update_item'         => __( 'Update Staff', 'euphoria-2016' ),
			'search_items'        => __( 'Search Staff', 'euphoria-2016' ),
			'not_found'           => __( 'Not Found', 'euphoria-2016' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'euphoria-2016' ),
		);

		$staff_args = array(
			'label'               => __( 'Staff Label', 'euphoria-2016' ),
			'description'         => __( 'euphoria Staff', 'euphoria-2016' ),
			'menu_icon'           => 'dashicons-id-alt',
			'labels'              => $staff_labels,
			'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields' ),
			'taxonomies'          => array(),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		register_post_type( 'events', $event_args );
		register_post_type( 'supporters', $supporter_args );
		register_post_type( 'talent', $talent_args );
		register_post_type( 'recipes', $recipe_args );
		register_post_type( 'venues', $venue_args );
		register_post_type( 'staff', $staff_args );
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies

		$talent_category_labels = array(
		  'name' => _x( 'Talent Cagtegories', 'taxonomy general name' ),
		  'singular_name' => _x( 'Talent Cagtegory', 'taxonomy singular name' ),
		  'search_items' =>  __( 'Search Talent Cagtegories' ),
		  'all_items' => __( 'All Talent Cagtegories' ),
		  'parent_item' => __( 'Parent Talent Cagtegory' ),
		  'parent_item_colon' => __( 'Parent Talent Cagtegory:' ),
		  'edit_item' => __( 'Edit Talent Cagtegory' ),
		  'update_item' => __( 'Update Talent Cagtegory' ),
		  'add_new_item' => __( 'Add New Talent Cagtegory' ),
		  'new_item_name' => __( 'New Talent Cagtegory Name' ),
		  'menu_name' => __( 'Talent Cagtegories' ),
		);

		register_taxonomy( 'talent-category', array('talent'), array(
		  'hierarchical' => true,
		  'labels' => $talent_category_labels,
		  'show_ui' => true,
		  'show_admin_column' => true,
		  'query_var' => true,
		  'rewrite' => array( 'slug' => 'talent-category' ),
		));

		$event_category_labels = array(
		  'name' => _x( 'Event Cagtegories', 'taxonomy general name' ),
		  'singular_name' => _x( 'Event Cagtegory', 'taxonomy singular name' ),
		  'search_items' =>  __( 'Search Event Cagtegories' ),
		  'all_items' => __( 'All Event Cagtegories' ),
		  'parent_item' => __( 'Parent Event Cagtegory' ),
		  'parent_item_colon' => __( 'Parent Event Cagtegory:' ),
		  'edit_item' => __( 'Edit Event Cagtegory' ),
		  'update_item' => __( 'Update Event Cagtegory' ),
		  'add_new_item' => __( 'Add New Event Cagtegory' ),
		  'new_item_name' => __( 'New Event Cagtegory Name' ),
		  'menu_name' => __( 'Event Cagtegories' ),
		);

		register_taxonomy( 'event-category', array('events'), array(
		  'hierarchical' => true,
		  'labels' => $event_category_labels,
		  'show_ui' => true,
		  'show_admin_column' => true,
		  'query_var' => true,
		  'rewrite' => array( 'slug' => 'event-category' ),
		));

		$recipe_category_labels = array(
		  'name' => _x( 'Recipe Cagtegories', 'taxonomy general name' ),
		  'singular_name' => _x( 'Recipe Cagtegory', 'taxonomy singular name' ),
		  'search_items' =>  __( 'Search Recipe Cagtegories' ),
		  'all_items' => __( 'All Recipe Cagtegories' ),
		  'parent_item' => __( 'Parent Recipe Cagtegory' ),
		  'parent_item_colon' => __( 'Parent Recipe Cagtegory:' ),
		  'edit_item' => __( 'Edit Recipe Cagtegory' ),
		  'update_item' => __( 'Update Recipe Cagtegory' ),
		  'add_new_item' => __( 'Add New Recipe Cagtegory' ),
		  'new_item_name' => __( 'New Recipe Cagtegory Name' ),
		  'menu_name' => __( 'Recipe Cagtegories' ),
		);

		register_taxonomy( 'recipe-category', array('recipes'), array(
		  'hierarchical' => true,
		  'labels' => $recipe_category_labels,
		  'show_ui' => true,
		  'show_admin_column' => true,
		  'query_var' => true,
		  'rewrite' => array( 'slug' => 'recipe-category' ),
		));

	  $support_level_labels = array(
	    'name' => _x( 'Support Levels', 'taxonomy general name' ),
	    'singular_name' => _x( 'Support Level', 'taxonomy singular name' ),
	    'search_items' =>  __( 'Search Support Levels' ),
	    'all_items' => __( 'All Support Levels' ),
	    'parent_item' => __( 'Parent Support Level' ),
	    'parent_item_colon' => __( 'Parent Support Level:' ),
	    'edit_item' => __( 'Edit Support Level' ),
	    'update_item' => __( 'Update Support Level' ),
	    'add_new_item' => __( 'Add New Support Level' ),
	    'new_item_name' => __( 'New Support Level Name' ),
	    'menu_name' => __( 'Support Levels' ),
	  );

	  register_taxonomy( 'support-levels', array('supporters'), array(
	    'hierarchical' => true,
	    'labels' => $support_level_labels,
	    'show_ui' => true,
	    'show_admin_column' => true,
	    'query_var' => true,
	    'rewrite' => array( 'slug' => 'support-levels' ),
	  ));

	  $ticket_packages = array(
	    'name' => _x( 'Ticket Packages', 'taxonomy general name' ),
	    'singular_name' => _x( 'Ticket Package', 'taxonomy singular name' ),
	    'search_items' =>  __( 'Search Ticket Packages' ),
	    'all_items' => __( 'All Ticket Packages' ),
	    'parent_item' => __( 'Parent Ticket Package' ),
	    'parent_item_colon' => __( 'Parent Ticket Package:' ),
	    'edit_item' => __( 'Edit Ticket Package' ),
	    'update_item' => __( 'Update Ticket Package' ),
	    'add_new_item' => __( 'Add New Ticket Package' ),
	    'new_item_name' => __( 'New Ticket Package Name' ),
	    'menu_name' => __( 'Ticket Packages' ),
	  );

	  register_taxonomy( 'ticket-packages', array('events'), array(
	    'hierarchical' => true,
	    'labels' => $ticket_packages,
	    'show_ui' => true,
	    'show_admin_column' => true,
	    'query_var' => true,
	    'rewrite' => array( 'slug' => 'ticket-packages' ),
	  ));

	}

	function add_options_page() {
		$option_args = array(

			/* (string) The title displayed on the options page. Required. */
			'page_title' => 'Options',

			/* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
			'menu_title' => '',

			/* (string) The slug name to refer to this menu by (should be unique for this menu).
			Defaults to a url friendly version of menu_slug */
			'menu_slug' => '',

			/* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
			Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
			'capability' => 'edit_posts',

			/* (int|string) The position in the menu order this menu should appear.
			WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
			Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
			Defaults to bottom of utility menu items */
			'position' => 20,

			/* (string) The slug of another WP admin page. if set, this will become a child page. */
			'parent_slug' => '',

			/* (string) The icon url for this menu. Defaults to default WordPress gear */
			'icon_url' => false,

			/* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists).
			If set to false, this parent page will appear alongside any child pages. Defaults to true */
			'redirect' => true,

			/* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2').
			Defaults to 'options'. Added in v5.2.7 */
			'post_id' => 'options',

			/* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up.
			Defaults to false. Added in v5.2.8. */
			'autoload' => false,
		);

		acf_add_options_page( $option_args );
	}

	function load_scripts() {
		wp_deregister_script('jquery');
		wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js", false, '', true);
		wp_enqueue_script('jquery');

		wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/static/javascripts/plugins/owl.carousel.js', array(), '1.0.0', true);

		wp_enqueue_script('cartjs', get_template_directory_uri() . '/static/javascripts/min/cart-min.js', array(), '1.0.0', true);
		wp_enqueue_script('sitejs', get_template_directory_uri() . '/static/javascripts/min/site-min.js', array(), '1.0.0', true);
	}




	function add_to_context( $context ) {
		$context['menu'] = new TimberMenu('header-menu');
		// $context['env'] = WP_ENV;

		$context['foo'] = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::get_context();';
		$context['site'] = $this;
		return $context;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter( 'myfoo', new Twig_Filter_Function( 'myfoo' ) );
		return $twig;
	}

}

new StarterSite();

function myfoo( $text ) {
	$text .= ' bar!';
	return $text;
}
