<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'archive-supporters.twig', 'archive.twig', 'index.twig' );

$data = Timber::get_context();

$data['title'] = post_type_archive_title( '', false );
array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );

$title_args = array(
	'posts_per_page' => -1,
	'post_type' => 'supporters',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'support-levels',
			'field'    => 'slug',
			'terms'    => 'title',
		),
	),
);

$diamond_args = array(
	'posts_per_page' => -1,
	'post_type' => 'supporters',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'support-levels',
			'field'    => 'slug',
			'terms'    => 'diamond',
		),
	),
);

$platinum_args = array(
	'posts_per_page' => -1,
	'post_type' => 'supporters',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'support-levels',
			'field'    => 'slug',
			'terms'    => 'platinum',
		),
	),
);

$gold_args = array(
	'posts_per_page' => -1,
	'post_type' => 'supporters',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'support-levels',
			'field'    => 'slug',
			'terms'    => 'gold',
		),
	),
);

$silver_args = array(
	'posts_per_page' => -1,
	'post_type' => 'supporters',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'support-levels',
			'field'    => 'slug',
			'terms'    => 'silver',
		),
	),
);

$bronze_args = array(
	'posts_per_page' => -1,
	'post_type' => 'supporters',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'support-levels',
			'field'    => 'slug',
			'terms'    => 'bronze',
		),
	),
);

$foodie_args = array(
	'posts_per_page' => -1,
	'post_type' => 'supporters',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'support-levels',
			'field'    => 'slug',
			'terms'    => 'foodie',
		),
	),
);

$restaurants_args = array(
	'posts_per_page' => -1,
	'post_type' => 'supporters',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'support-levels',
			'field'    => 'slug',
			'terms'    => 'restaurants',
		),
	),
);

$media_args = array(
	'posts_per_page' => -1,
	'post_type' => 'supporters',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'support-levels',
			'field'    => 'slug',
			'terms'    => 'media',
		),
	),
);

// get the body copy out the *page* "Supporters"
$supporter_args = array (
	'pagename' => 'Supporters',
);

$supporter     = new WP_Query( $supporter_args );
$title    = new WP_Query( $title_args );
$diamond       = new WP_Query( $diamond_args );
$platinum      = new WP_Query( $platinum_args );
$gold          = new WP_Query( $gold_args );
$silver        = new WP_Query( $silver_args );
$bronze        = new WP_Query( $bronze_args );
$foodie        = new WP_Query( $foodie_args );
$restaurants   = new WP_Query( $restaurants_args );
$media         = new WP_Query( $media_args );

$data['supporters_page_subheading'] = get_field('supporters_page_subheading', 'options');

$data['posts']        = Timber::get_posts();
$data['supporter']    = Timber::get_posts($supporter);
$data['title']     = Timber::get_posts($title);
$data['diamond']      = Timber::get_posts($diamond);
$data['platinum']     = Timber::get_posts($platinum);
$data['gold']         = Timber::get_posts($gold);
$data['silver']       = Timber::get_posts($silver);
$data['bronze']       = Timber::get_posts($bronze);
$data['foodie']       = Timber::get_posts($foodie);
$data['restaurants']  = Timber::get_posts($restaurants);
$data['media']        = Timber::get_posts($media);
$data['featured_supporters'] = get_field('featured_sponsors', 'options');

Timber::render( $templates, $data );
