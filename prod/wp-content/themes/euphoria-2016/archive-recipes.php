<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'archive-recipes.twig', 'archive.twig', 'index.twig' );

$data = Timber::get_context();

$data['title'] = post_type_archive_title( '', false );
array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );

$lunch = array(
	'post_type' => 'recipes',
	'tax_query' => array(
		array(
			'taxonomy' => 'recipe-category',
			'field'    => 'slug',
			'terms'    => 'lunch',
		),
	),
);

$dinner = array(
	'post_type' => 'recipes',
	'tax_query' => array(
		array(
			'taxonomy' => 'recipe-category',
			'field'    => 'slug',
			'terms'    => 'dinner',
		),
	),
);

$recipe_page = array (
	'pagename' => 'Recipes',
);


$recipe = new WP_Query( $recipe_page );
$lunch     = new WP_Query( $lunch );
$dinner    = new WP_Query( $dinner );

$data['posts']  = Timber::get_posts();
$data['page']   = Timber::get_posts($recipe);
$data['lunch']  = Timber::get_posts($lunch);
$data['dinner'] = Timber::get_posts($dinner);

Timber::render( $templates, $data );
