/* global $, eztixKioskRemote */
function Cart() {
    var events = [];
    var $cartItem;
    var initialLoad = true;

    this.init = function () {
        // Get all events
        events = eztixKioskRemote.getAllEventData();

        bindAddToCart();
        bindCheckout();
        loadCart();
        loadCartItemTemplate();
    };

    //////////

    function addToCart(e) {
        e.preventDefault();

        var id = $(this).data('eztixId');
        var event = getEvent(id);
        var ticket;

        // Mark button as adding
        $(this).addClass('is-adding');

        // Add a ticket to the cart
        if (event && event.tickets && event.tickets[0]) {
            ticket = event.tickets[0];

            if (eztixKioskRemote.setTicketQuantity(ticket.ticket_id, 1)) {
                eztixKioskRemote.submitCart(updateCartView);
            }
        }
    }

    function updateCartView() {
        var $ticketView;
        var ticketCount = 0;

        // Remove all adding statuses from buttons
        $('.is-adding').removeClass('is-adding');

        // Get cart
        var cart = eztixKioskRemote.getCart();

        // Clear all cart items from view
        $('.cart__item').remove();

        // Remove in cart statuses
        $('.in-cart').removeClass('in-cart').html('ADD TICKET TO CART');

        // Add cart items
        if (cart.tickets && Object.keys(cart.tickets).length > 0) {
            // Clear empty cart message
            $('.cart__empty').remove();

            // Place cart items and increment ticket count
            $.each(cart.tickets, function (key, ticket) {
                $ticketView = $cartItem.clone();

                drawCartTicket($ticketView, ticket);

                $ticketView.insertBefore('.cart__total');

                $('[data-eztix-id=' + ticket.ticket_event_id + ']').addClass('in-cart').html('TICKET ADDED');

                ticketCount += ticket.ticket_quantity;
            });
        }

        // Add cart total
        $('#js-cart-total-price').html('$' + cart.financials.subtotal);

        // Add cart item total
        $('#js-cart-item-count')
            .prop('data-item-count', ticketCount)
            .html(ticketCount);
    }

    function drawCartTicket($ticketView, ticket) {
        // Update content
        $('.table__column--qty .h3', $ticketView).html(ticket.ticket_quantity);
        $('.table__column--ticket .h3', $ticketView).html(ticket.ticket_event_title);
        $('.table__column--price .h3', $ticketView).html(ticket.ticket_sub_total);

        // Bind increase arrow
        $('.js-increase-qty', $ticketView).on('click', function (e) {
            e.preventDefault();

            if (eztixKioskRemote.increaseTicketQuantity(ticket.ticket_id)) {
                eztixKioskRemote.submitCart(function () {
                    updateCartView();
                });
            }
        });

        // Bind decrease arrow
        $('.js-decrease-qty', $ticketView).on('click', function (e) {
            e.preventDefault();

            if (eztixKioskRemote.decreaseTicketQuantity(ticket.ticket_id)) {
                eztixKioskRemote.submitCart(function () {
                    updateCartView();
                });
            }
        });
    }

    function getEvent(id) {
        var event = null;

        // Search for event
        $.each(events, function (key, value) {
            if (value.event_id == id) {
                event = value;
                return false;
            }
        });

        return event;
    }

    function bindAddToCart() {
        $('[data-eztix-id]').on('click', addToCart);
    }

    function bindCheckout() {
        $('#js-launch-eztix').on('click', function (e) {
            e.preventDefault();

            // Send to checkout page
            eztixKioskRemote.checkout();
        });
    }

    function loadCart() {
        if (initialLoad) {
            // Load the cart
            eztixKioskRemote.loadCart(function () {
                updateCartView();
            });

            initialLoad = false;
        }
    }

    function loadCartItemTemplate() {
        // Get the cart item template
        $cartItem = $('.table--cart .cart__item').remove();
    }
}

Cart.prototype.init = function () {
    this.init();
};


