/* global Cart, eztixKioskRemote */
jQuery(document).ready(function ($) {
    'use strict';

    $('.js-page-nav').on('click', function(e) {
        e.preventDefault();

        var target = $(this).attr('href');

        window.scrollTo(0, $(target).offset().top - 60)
    });

    if ( $('.js-equal-heights .card').length && window.innerWidth >= 900 ) {
        var h = 0;

        setTimeout(function() {
            $('.card').each(function() {
                var $this = $(this);

                if ( $this.outerHeight(true) > h ) {
                    h = $this.outerHeight(true);
                };
            });

            $('.card').css({
                'height': (h + 20) + "px"
            });
        }, 300)
    }

    var addIsActive = function (targ) {
        $(targ).addClass('is-active');
    };

    if ($('.nav').length) {
        var url = location.href.split('?')[0];
        addIsActive( $('[href="' + url + '"]').parent('li') );

        $('.nav--page a').on('click', function () {
            $('.nav--page a').removeClass('is-active');
            addIsActive(this);
        });
    }

    $('#js-nav-toggle').on('click', function () {
        if ($(this).hasClass('open')) {
            $(this).addClass('close');
            $(this).removeClass('open');
            $('body').removeClass('nav-is-open');
        } else if ($(this).hasClass('close')) {
            $(this).addClass('open');
            $(this).removeClass('close');
            $('body').addClass('nav-is-open');
        }
    });

    var owl = $('.owl-carousel');
    owl.owlCarousel({
        autoHeight: true,
        margin: 36,
        nav: true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1,
                stagePadding: 72
            },
            1180: {
                items: 2,
                stagePadding: 72 + 57
            }
        }
    });

    $('.owl-carousel--full').owlCarousel({
        animateOut: 'fadeOut',
        smartSpeed: 350,
        nav: true,
        autoHeight:true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },

            768: {
                items: 1

            }
        }
    });

    // ============================================================
    // Cart
    // ============================================================

    $('body').on('eztixKioskRemote_ready', function () {
        var producerId = 1735;
        var eventId = 688138;

        // Initialize EZTix
        eztixKioskRemote.init(producerId, eventId);

        var cart = new Cart();

        // Load ticket status
        eztixKioskRemote.loadTicketStatus(cart.init);
    });



    $("#js-cart-button").on('click', function () {
        $(this).addClass('opened');

        $("#js-cart-list").addClass('is-visible');
    });

    var closePopup = function () {
        $(this).parent().removeClass('is-visible');
    };

    $("#js-close-popup").on('click', closePopup);
});
