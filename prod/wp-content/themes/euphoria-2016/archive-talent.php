<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'archive-talent.twig', 'archive.twig', 'index.twig' );

$data = Timber::get_context();

$data['title'] = post_type_archive_title( '', false );
array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );

$chef_args = array(
	'posts_per_page' => -1,
	'orderby'=> 'title',
	'order' => 'ASC',
	'post_type' => 'talent',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'talent-category',
			'field'    => 'slug',
			'terms'    => 'chef',
		),
	),
);

$musician_args = array(
	'posts_per_page' => -1,
	'orderby'=> 'title',
	'order' => 'ASC',
	'post_type' => 'talent',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'talent-category',
			'field'    => 'slug',
			'terms'    => 'musician',
		),
	),
);

$drink_expert_args = array(
	'posts_per_page' => -1,
	'orderby'=> 'title',
	'order' => 'ASC',
	'post_type' => 'talent',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'talent-category',
			'field'    => 'slug',
			'terms'    => 'drink-expert',
		),
	),
);

$participating_media_args = array(
	'posts_per_page' => -1,
	'orderby'=> 'title',
	'order' => 'ASC',
	'post_type' => 'talent',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'talent-category',
			'field'    => 'slug',
			'terms'    => 'participating-media',
		),
	),
);

$chef                = new WP_Query( $chef_args );
$musician            = new WP_Query( $musician_args );
$drink_expert        = new WP_Query( $drink_expert_args );
$participating_media = new WP_Query( $participating_media_args );

$data['talent_page_subheading'] = get_field('talent_page_subheading', 'options');

$data['posts']    = Timber::get_posts();
$data['chef']     = Timber::get_posts($chef);
$data['musician'] = Timber::get_posts($musician);
$data['drink']    = Timber::get_posts($drink_expert);
$data['media']    = Timber::get_posts($participating_media);

Timber::render( $templates, $data );
