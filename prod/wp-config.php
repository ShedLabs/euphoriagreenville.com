<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'euphoria_local');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N9~Lvs-G$C|e>d+AagP#wO-DiX(;44,-+|;)VZ?|+EX+s/+-rF-WfsOJKzgZgpV8');
define('SECURE_AUTH_KEY',  'k1yA5m BR$0V4~W8s!>i4+T%LCU`-2@R?<VdV|rw[,,G2Bv_tg{3<i^2Pg-N6?E[');
define('LOGGED_IN_KEY',    'U8E<JiBXH.|[V)#u~7Wl[[5DmNI|R2?D+h}4$2cBG<jW[D<B(<z5/Y/2(^fP>>Yv');
define('NONCE_KEY',        'j0z]!VS25.5LcOngu&,*%P68l&BdS$yAG.KrvTRkZ*4pX}XzJ+3?hFC2txF^5aqP');
define('AUTH_SALT',        'mS4zfybyvrbAzuY-5&VBt1xwf*?HFS B:zMSS5.IhbYwSph8N7l=H1?JTZAA1iwN');
define('SECURE_AUTH_SALT', '`+tHzr9+9?x4QNV -jc{7[%p~ZZxTpnf>XizQCc])+/rv/U=9_o&OtyT>}IT(dLH');
define('LOGGED_IN_SALT',   '^Q- be|<-z-lCE8*>QT`FH4f+;uvS|[y45WD6U7Q~pd>[rlNIh$/w|A=Lz5.Hm?!');
define('NONCE_SALT',       '|5tt/7(DV].B+T?`,*3^d|(+S`Y3s-8@Y9]/RV|-OqZS9;PgbDG^&n_Nx2(5S[QN');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
